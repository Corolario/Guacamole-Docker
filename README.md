## Instala o Guacamole em Docker

`wget https://gitlab.com/Corolario/Guacamole-Docker/raw/master/docker-compose.yml && docker-compose up -d`


Cria certificado SSL

`mkdir certs && cd certs &&
openssl req -x509 -nodes -days 90 -newkey rsa:2048 -subj "/C=BR/ST=SP/L=SaoPaulo/O=localhost/OU=TI/CN=localhost" -keyout priv.key -out cert.crt`